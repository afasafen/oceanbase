#pragma once

 #include "lib/lock/mutex.h"
 #include "lib/file/ob_file.h"
 #include "lib/timezone/ob_timezone_info.h"
 #include "sql/engine/cmd/ncre_utils.h"
 #include "sql/engine/cmd/ob_load_data_impl.h"
 #include "sql/engine/cmd/ob_load_data_parser.h"
 #include "storage/blocksstable/ob_index_block_builder.h"
 #include "storage/ob_parallel_external_sort.h"
 #include "storage/tx_storage/ob_ls_handle.h"
 #include <atomic>
#include <sys/types.h>

 namespace oceanbase
 {
 namespace sql
 {
 
 const int32_t kConsumerNum = 8;
 const int32_t kConsumerNumSstable = 8;
 const int32_t MacroAlloacatorSize = (1 << 28);
 const int64_t MaxTimeout = (1 << 30);
 const int64_t BigOffset = (1 << 29);

 class ObLoadDataBuffer
 {
 public:
   ObLoadDataBuffer();
   ~ObLoadDataBuffer();
   void reuse();
   void reset();
   int create(int64_t capacity);
   int squash();
   OB_INLINE char *data() const { return data_; }
   OB_INLINE char *begin() const { return data_ + begin_pos_; }
   OB_INLINE char *end() const { return data_ + end_pos_; }
   OB_INLINE bool empty() const { return end_pos_ == begin_pos_; }
   OB_INLINE int64_t get_data_size() const { return end_pos_ - begin_pos_; }
   OB_INLINE int64_t get_remain_size() const { return capacity_ - end_pos_; }
   OB_INLINE void consume(int64_t size) { begin_pos_ += size; }
   OB_INLINE void produce(int64_t size) { end_pos_ += size; }
   OB_INLINE void clear() { begin_pos_ = end_pos_ = 0;}
 private:
   common::ObArenaAllocator allocator_;
   char *data_;
   int64_t begin_pos_;
   int64_t end_pos_;
   int64_t capacity_;
 };

 class ObCircularBuffer
 {
 public:
  ObCircularBuffer();
  ~ObCircularBuffer();
  int create(int64_t capacity);
  void consume();
  void reset();
  bool empty() const { return empty_; }
  bool full() const { return full_; }

  template<typename T>
  int64_t write(const T& item)
  {
   empty_ = false;
   if (full_) 
   {
    return 0;
   }
   const int64_t item_size = sizeof(T) + item.get_deep_copy_size();
   if (read_ <= write_)
   {
    int64_t left = buf_len_ - write_;
    if (item_size + sizeof(int64_t) < left)
    {
     MEMCPY(buf_ + write_, &item_size, sizeof(int64_t));
     T *new_item = new (buf_ + write_ + sizeof(int64_t)) T();
     int64_t buf_pos = sizeof(T);
     new_item->deep_copy(item, buf_ + write_ + sizeof(int64_t), item_size, buf_pos);
     write_ += item_size + sizeof(int64_t);
     return item_size;
    }
    else
    {
     if (read_ < item_size + sizeof(int64_t)) return 0;
     boundary_ = write_;
     write_ = 0;
     MEMCPY(buf_ + write_, &item_size, sizeof(int64_t));
     T *new_item = new (buf_ + write_ + sizeof(int64_t)) T();
     int64_t buf_pos = sizeof(T);
     new_item->deep_copy(item, buf_ + write_ + sizeof(int64_t), item_size, buf_pos);
     write_ = item_size + sizeof(int64_t);
     full_ = (write_ == read_);
     return item_size;
    }
   }
   else
   {
    int64_t left = read_ - write_;
    if (left < item_size + sizeof(int64_t)) return 0;
    MEMCPY(buf_ + write_, &item_size, sizeof(int64_t));
    T *new_item = new (buf_ + write_ + sizeof(int64_t)) T();
    int64_t buf_pos = sizeof(T);
    new_item->deep_copy(item, buf_ + write_ + sizeof(int64_t), item_size, buf_pos);
    write_ += item_size + sizeof(int64_t);
    full_ = (write_ == read_);
    return item_size;
   }
  }

 template<typename T>
 int64_t read(T *item)
 {
  full_ = false;
  if (empty_)
  {
   return 0;
  }
  else
  {
   if (read_ == boundary_)
   {
    read_ = 0;
    boundary_ = buf_len_;
   }
   const T *src = reinterpret_cast<T *>(buf_ + read_ + sizeof(int64_t));
   char *dest = reinterpret_cast<char *>(item);

   const int64_t item_size = sizeof(T) + src->get_deep_copy_size();
   T *new_item = new (dest) T();
   int64_t buf_pos = sizeof(T);
   new_item->deep_copy(*src, dest, item_size, buf_pos);

   int64_t count = *(int64_t *)(buf_ + read_);
   read_ += count + sizeof(int64_t);
   empty_ = (read_ == write_);
   return count;
  }
 }

 private:
  common::ObArenaAllocator allocator_;
  char *buf_;
  int64_t buf_len_;
  int64_t read_;
  int64_t write_;
  bool full_;
  bool empty_;
  int64_t boundary_;
 };

 class ObBlockingDataBuffer
 {
 public:
  ObBlockingDataBuffer(int64_t capacity);
  ~ObBlockingDataBuffer() = default;
  void shutdown();

  template<typename T>
  void write(const T& item) 
  {
    ncre::NcreMutexGuard guard(mutex_);
    // LOG_INFO("begin to write to blocking buffer");
    while (buffer_.write(item) == 0)
    {
      // LOG_WARN("wait for writing to blocking buffer");
      not_full_.wait();
      if (shutdown_) return;
    }
    not_empty_.signal();
    // LOG_INFO("finish writing to blocking buffer");
  }

 template<typename T>
 int64_t read(T *item)
 {
   ncre::NcreMutexGuard guard(mutex_);
  //  LOG_INFO("begin to read from blocking buffer");
   int64_t count = 0;
   if (shutdown_ && buffer_.empty()) return 0;
   while (0 == (count = buffer_.read(item)))
   {
    //  LOG_WARN("wait for read from blocking buffer");
     not_empty_.wait();
     if (shutdown_ && buffer_.empty()) return 0;
   }
  //  LOG_INFO("finish reading from blocking buffer");
   not_full_.signal();
   return count;
 }

 private:
  ObCircularBuffer buffer_;
  ncre::NcreMutex mutex_;
  ncre::NcreCond not_empty_;
  ncre::NcreCond not_full_;
  bool shutdown_;
 };

 class ObLoadSequentialFileReader
 {
 public:
   ObLoadSequentialFileReader();
   ~ObLoadSequentialFileReader();
   int open(const ObString &filepath);
   int read_next_buffer(ObLoadDataBuffer &buffer);
   int read_next_squence(ObLoadDataBuffer &buffer, int64_t &off, bool is_first);
   void set_off(int64_t off) {offset_ = off; is_read_end_ = false;}
   void set_end_off(int64_t off) {end_off_ = off;}
 private:
   common::ObFileReader file_reader_;
   int64_t offset_;
   int64_t end_off_;
   bool is_read_end_;
 };

 class ObLoadCSVPaser
 {
 public:
   ObLoadCSVPaser();
   ~ObLoadCSVPaser();
   void reset();
   int init(const ObDataInFileStruct &format, int64_t column_count,
            common::ObCollationType collation_type);
   int get_next_row(ObLoadDataBuffer &buffer, const common::ObNewRow *&row);
   void wait_by_producer(int32_t consumer_num);
   void claim_no_more_data();
   bool wait_by_consumer1(int32_t consumer_num);
   bool wait_by_consumer2();
 private:
   struct UnusedRowHandler
   {
     int operator()(common::ObIArray<ObCSVGeneralParser::FieldValue> &fields_per_line)
     {
       UNUSED(fields_per_line);
       return OB_SUCCESS;
     }
   };
 private:
   common::ObArenaAllocator allocator_;
   common::ObCollationType collation_type_;
   ObCSVGeneralParser csv_parser_;
   common::ObNewRow row_;
public:
   ncre::NcreMutex mutex_;
private:
   ncre::NcreCond producer_cond_;
   ncre::NcreCond consumer_cond_;
   int32_t consumer_num_;
   bool no_more_data_;
   bool started_;
   UnusedRowHandler unused_row_handler_;
   common::ObSEArray<ObCSVGeneralParser::LineErrRec, 1> err_records_;
   bool is_inited_;
 };

 class ObLoadDatumRow
 {
   OB_UNIS_VERSION(1);
 public:
   ObLoadDatumRow();
   ~ObLoadDatumRow();
   void reset();
   int init(int64_t capacity);
   int64_t get_deep_copy_size() const;
   int deep_copy(const ObLoadDatumRow &src, char *buf, int64_t len, int64_t &pos);
   OB_INLINE bool is_valid() const { return count_ > 0 && nullptr != datums_; }
   DECLARE_TO_STRING;
 public:
   common::ObArenaAllocator allocator_;
   int64_t capacity_;
   int64_t count_;
   blocksstable::ObStorageDatum *datums_;
 };

 class ObLoadDatumRowCompare
 {
 public:
   ObLoadDatumRowCompare();
   ~ObLoadDatumRowCompare();
   int init(int64_t rowkey_column_num, const blocksstable::ObStorageDatumUtils *datum_utils);
   bool operator()(const ObLoadDatumRow *lhs, const ObLoadDatumRow *rhs);
   int get_error_code() const { return result_code_; }
 public:
   int result_code_;
   int64_t rowkey_column_num_;
   const blocksstable::ObStorageDatumUtils *datum_utils_;
  //  blocksstable::ObDatumRowkey lhs_rowkey_;
  //  blocksstable::ObDatumRowkey rhs_rowkey_;
   bool is_inited_;
 };

 class ObLoadRowCaster
 {
 public:
   ObLoadRowCaster();
   ~ObLoadRowCaster();
   int init(const share::schema::ObTableSchema *table_schema,
            const common::ObIArray<ObLoadDataStmt::FieldOrVarStruct> &field_or_var_list);
   int get_casted_row(const common::ObNewRow &new_row, const ObLoadDatumRow *&datum_row);
 private:
   int init_column_schemas_and_idxs(
     const share::schema::ObTableSchema *table_schema,
     const common::ObIArray<ObLoadDataStmt::FieldOrVarStruct> &field_or_var_list);
   int cast_obj_to_datum(const share::schema::ObColumnSchemaV2 *column_schema,
                         const common::ObObj &obj, blocksstable::ObStorageDatum &datum);
 private:
   common::ObArray<const share::schema::ObColumnSchemaV2 *> column_schemas_;
   common::ObArray<int64_t> column_idxs_; // Mapping of store columns to source data columns
   int64_t column_count_;
   common::ObCollationType collation_type_;
   ObLoadDatumRow datum_row_;
   common::ObArenaAllocator cast_allocator_;
   common::ObTimeZoneInfo tz_info_;
   bool is_inited_;
 };

 class ObLoadExternalSort
 {
 public:
   ObLoadExternalSort();
   ~ObLoadExternalSort();
   int init(const share::schema::ObTableSchema *table_schema, int64_t mem_size,
            int64_t file_buf_size);
   int append_row(const ObLoadDatumRow &datum_row);
   int close();
   int get_next_row(const ObLoadDatumRow *&datum_row);
 public:
   common::ObArenaAllocator allocator_;
   blocksstable::ObStorageDatumUtils datum_utils_;
   ncre::NcreMutex mutex_;
   ObLoadDatumRowCompare compare_;
   storage::ObExternalSort<ObLoadDatumRow, ObLoadDatumRowCompare> external_sort_;
   bool is_closed_;
   bool is_inited_;
 };

 class ObLoadSSTableWriter
 {
 public:
   ObLoadSSTableWriter();
   ~ObLoadSSTableWriter();
   int init(const share::schema::ObTableSchema *table_schema);
  //  int append_row(const ObLoadDatumRow &datum_row, int cur);
   int close();
 private:
   int init_sstable_index_builder(const share::schema::ObTableSchema *table_schema);
   int init_macro_block_writer(const share::schema::ObTableSchema *table_schema);
   int create_sstable();
 public:
   ncre::NcreMutex mutex_;
   std::atomic<bool> open_;
   ObLightyQueue priority_queue_[kConsumerNumSstable];
   ObLightyQueue close_queue_[kConsumerNumSstable];
   common::ObTabletID tablet_id_;
   storage::ObTabletHandle tablet_handle_;
   share::ObLSID ls_id_;
   storage::ObLSHandle ls_handle_;
   int64_t rowkey_column_num_;
   int64_t extra_rowkey_column_num_;
   int64_t column_count_;
   storage::ObITable::TableKey table_key_;
   blocksstable::ObSSTableIndexBuilder sstable_index_builder_;
   blocksstable::ObDataStoreDesc data_store_desc_;
   ObLightyQueue free_queue_;
   ObLightyQueue full_queue_;
   bool is_finished = false;
   blocksstable::ObDatumRow datum_row_;
   bool is_closed_;
   bool is_inited_;
 };

 class ObLoadDataDirectDemo : public ObLoadDataBase
 {
   static const int64_t MEM_BUFFER_SIZE = (10LL << 30); // 1G
   static const int64_t FILE_BUFFER_SIZE = (2LL << 15); // 2M
 public:
   friend void produce_row(ObLoadDataDirectDemo *demo);
   friend void produce_datum(ObLoadDataDirectDemo *demo, int cur);
   friend void consume_datum(ObLoadSSTableWriter &sstable_writer, int cur, ObFragmentMerge<ObLoadDatumRow, ObLoadDatumRowCompare> *merge);
   ObLoadDataDirectDemo();
   virtual ~ObLoadDataDirectDemo();
   int execute(ObExecContext &ctx, ObLoadDataStmt &load_stmt) override;
 private:
   int inner_init(ObLoadDataStmt &load_stmt);
   int do_load();
 private:
   ObLoadCSVPaser csv_parser_[kConsumerNum + 1];
   ObLoadSequentialFileReader file_reader_[kConsumerNum + 1];
   ObLoadDataBuffer buffer_[kConsumerNum + 1];
   ObLoadRowCaster row_caster[kConsumerNum];
   ObLightyQueue cur_queue;
   ObLoadExternalSort external_sort_[kConsumerNum + 1];
   ObLoadSSTableWriter sstable_writer_;
 };

 void produce_row(ObLoadDataDirectDemo *demo);
 void produce_datum(ObLoadDataDirectDemo *demo, int cur);
 void consume_datum(ObLoadSSTableWriter &sstable_writer, int cur, ObFragmentMerge<ObLoadDatumRow, ObLoadDatumRowCompare> *merge);
 } // namespace sql
 } // namespace oceanbase