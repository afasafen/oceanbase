#pragma once
 #include <lib/thread/thread.h>
 #include <lib/thread/threads.h>
 #include "lib/file/ob_file.h"
#include <sys/types.h>
namespace ncre {
class NcreMutex
 {
 public:
  NcreMutex()
  {
    pthread_mutex_init(&mutex_, NULL);
  }
  ~NcreMutex()
  {
    pthread_mutex_destroy(&mutex_);
  }
  void lock()
  {
    pthread_mutex_lock(&mutex_);
  }
  void unlock()
  {
    pthread_mutex_unlock(&mutex_);
  }
 private:
  friend class NcreCond;
  pthread_mutex_t mutex_;
 };

 class NcreMutexGuard
 {
 public:
  NcreMutexGuard(NcreMutex& mutex) : mutex_(mutex)
  {
    mutex_.lock();
  }
  ~NcreMutexGuard()
  {
    mutex_.unlock();
  }
 private:
  NcreMutex& mutex_;
 };

 class NcreCond
 {
 public:
  NcreCond(NcreMutex& mutex) : mutex_(mutex)
  {
   pthread_cond_init(&pcond_, NULL);
  }
  ~NcreCond()
  {
   pthread_cond_destroy(&pcond_);
  }
  void wait()
  {
   pthread_cond_wait(&pcond_, &mutex_.mutex_);
  }
  void signal()
  {
   pthread_cond_signal(&pcond_);
  }
  void broadcast()
  {
   pthread_cond_broadcast(&pcond_);
  }
 private:
  NcreMutex& mutex_;
  pthread_cond_t pcond_;
 };

 class NcreThread: public oceanbase::lib::Threads
 {
 public:
  NcreThread();
  NcreThread(oceanbase::lib::Thread::Runnable &job);
  void run(int64_t) final;
  void set_job(oceanbase::lib::Thread::Runnable &job);

 private:
  oceanbase::lib::Thread::Runnable job_;
 };
};