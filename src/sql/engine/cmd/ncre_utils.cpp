#include "ncre_utils.h"
#include "share/rc/ob_tenant_base.h"

using namespace ncre;
using namespace oceanbase;

NcreThread::NcreThread()
  : lib::Threads()
{
 set_run_wrapper(MTL_CTX());
}

NcreThread::NcreThread(lib::Thread::Runnable &job)
  : lib::Threads()
{
 job_ = job;
 set_run_wrapper(MTL_CTX());
}

void NcreThread::set_job(lib::Thread::Runnable &job) {
  job_ = job;
}

void NcreThread::run(int64_t)
{
 job_();
}