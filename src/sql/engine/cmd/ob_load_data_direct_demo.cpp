
#include "lib/lock/mutex.h"
#include "lib/lock/ob_lock_guard.h"
#include "lib/ob_errno.h"
#include "lib/oblog/ob_log_module.h"
#include "lib/thread/thread.h"
#include <functional>
#define USING_LOG_PREFIX SQL_ENG

#include "sql/engine/cmd/ob_load_data_direct_demo.h"
#include "sql/engine/cmd/ncre_utils.h"
#include "observer/omt/ob_tenant_timezone_mgr.h"
#include "share/schema/ob_schema_getter_guard.h"
#include "share/tablet/ob_tablet_to_ls_operator.h"
#include "storage/tablet/ob_tablet_create_delete_helper.h"
#include "storage/tx_storage/ob_ls_service.h"

namespace oceanbase
{
namespace sql
{
using namespace blocksstable;
using namespace common;
using namespace lib;
using namespace observer;
using namespace share;
using namespace share::schema;

/**
 * ObLoadDataBuffer
 */

ObLoadDataBuffer::ObLoadDataBuffer()
  : allocator_(ObModIds::OB_SQL_LOAD_DATA), data_(nullptr), begin_pos_(0), end_pos_(0), capacity_(0)
{
}

ObLoadDataBuffer::~ObLoadDataBuffer()
{
  reset();
}

void ObLoadDataBuffer::reuse()
{
  begin_pos_ = 0;
  end_pos_ = 0;
}

void ObLoadDataBuffer::reset()
{
  allocator_.reset();
  data_ = nullptr;
  begin_pos_ = 0;
  end_pos_ = 0;
  capacity_ = 0;
}

int ObLoadDataBuffer::create(int64_t capacity)
{
  int ret = OB_SUCCESS;
  if (OB_UNLIKELY(nullptr != data_)) {
    ret = OB_INIT_TWICE;
    LOG_WARN("ObLoadDataBuffer init twice", KR(ret), KP(this));
  } else if (OB_UNLIKELY(capacity <= 0)) {
    ret = OB_INVALID_ARGUMENT;
    LOG_WARN("invalid args", KR(ret), K(capacity));
  } else {
    allocator_.set_tenant_id(MTL_ID());
    if (OB_ISNULL(data_ = static_cast<char *>(allocator_.alloc(capacity)))) {
      ret = OB_ALLOCATE_MEMORY_FAILED;
      LOG_WARN("fail to alloc memory", KR(ret), K(capacity));
    } else {
      capacity_ = capacity;
    }
  }
  return ret;
}

int ObLoadDataBuffer::squash()
{
  int ret = OB_SUCCESS;
  if (OB_UNLIKELY(nullptr == data_)) {
    ret = OB_NOT_INIT;
    LOG_WARN("ObLoadDataBuffer not init", KR(ret), KP(this));
  } else {
    const int64_t data_size = get_data_size();
    if (data_size > 0) {
      MEMMOVE(data_, data_ + begin_pos_, data_size);
    }
    begin_pos_ = 0;
    end_pos_ = data_size;
  }
  return ret;
}

ObCircularBuffer::ObCircularBuffer()
  : allocator_(ObModIds::OB_SQL_LOAD_DATA), buf_(nullptr), buf_len_(0), read_(0), write_(0), full_(false), empty_(false), boundary_(0)
{
}

ObCircularBuffer::~ObCircularBuffer()
{
  reset();
}

void ObCircularBuffer::reset()
{
  allocator_.reset();
  buf_ = nullptr;
  buf_len_ = 0;
  read_ = 0;
  write_ = 0;
  full_ = false;
  empty_ = false;
  boundary_ = 0;
}

int ObCircularBuffer::create(int64_t capacity) 
{
  int ret = OB_SUCCESS;
  if (OB_UNLIKELY(nullptr != buf_)) {
    ret = OB_INIT_TWICE;
    LOG_WARN("ObCircularBuffer init twice", KR(ret), KP(this));
  } else if (OB_UNLIKELY(capacity <= 0)) {
    ret = OB_INVALID_ARGUMENT;
    LOG_WARN("invalid args", KR(ret), K(capacity));
  } else {
    allocator_.set_tenant_id(MTL_ID());
    if (OB_ISNULL(buf_ = static_cast<char *>(allocator_.alloc(capacity)))) {
      ret = OB_ALLOCATE_MEMORY_FAILED;
      LOG_WARN("fail to alloc memory", KR(ret), K(capacity));
    } else {
      read_ = write_ = 0;
      empty_ = true;
      full_ = false;
      boundary_ = buf_len_ = capacity;
    }
  }
  return ret;
}

ObBlockingDataBuffer::ObBlockingDataBuffer(int64_t capacity)
  : buffer_(), mutex_(), not_empty_(mutex_), not_full_(mutex_), shutdown_(false)
{
  // LOG_INFO("create blocking buffer");
  buffer_.create(capacity);
}

void ObBlockingDataBuffer::shutdown()
{
  ncre::NcreMutexGuard guard(mutex_);
  // LOG_INFO("shutdown blocking buffer");
  shutdown_ = true;
  not_empty_.broadcast();
  not_full_.broadcast();
}

/**
 * ObLoadSequentialFileReader
 */

ObLoadSequentialFileReader::ObLoadSequentialFileReader()
  : offset_(0), is_read_end_(false)
{
}

ObLoadSequentialFileReader::~ObLoadSequentialFileReader()
{
}

int ObLoadSequentialFileReader::open(const ObString &filepath)
{
  int ret = OB_SUCCESS;
  if (OB_FAIL(file_reader_.open(filepath, false))) {
    LOG_WARN("fail to open file", KR(ret));
  }
  return ret;
}

int ObLoadSequentialFileReader::read_next_buffer(ObLoadDataBuffer &buffer)
{
  int ret = OB_SUCCESS;
  if (OB_UNLIKELY(!file_reader_.is_opened())) {
    ret = OB_FILE_NOT_OPENED;
    LOG_WARN("file not opened", KR(ret));
  } else if (is_read_end_) {
    ret = OB_ITER_END;
  } else if (OB_LIKELY(buffer.get_remain_size() > 0)) {
    const int64_t buffer_remain_size = (buffer.get_remain_size() > (end_off_ - offset_))?(end_off_ - offset_):buffer.get_remain_size();
    int64_t read_size = 0;
    if (OB_FAIL(file_reader_.pread(buffer.end(), buffer_remain_size, offset_, read_size))) {
      LOG_WARN("fail to do pread", KR(ret));
    } else if (read_size == 0) {
      is_read_end_ = true;
      ret = OB_ITER_END;
    } else {
      offset_ += read_size;
      buffer.produce(read_size);
    }
  }
  return ret;
}

int ObLoadSequentialFileReader::read_next_squence(ObLoadDataBuffer &buffer, int64_t &off, bool is_first) {
  offset_ += BigOffset;
  int ret = OB_SUCCESS;
  buffer.clear();
  if (OB_UNLIKELY(!file_reader_.is_opened())) {
    ret = OB_FILE_NOT_OPENED;
    LOG_WARN("file not opened", KR(ret));
  } else if (is_read_end_) {
    ret = OB_ITER_END;
  } else if (OB_LIKELY(buffer.get_remain_size() > 0)) {
    if(is_first) {
      const int64_t buffer_remain_size = buffer.get_remain_size();
      int64_t read_size = 0;
      if (OB_FAIL(file_reader_.pread(buffer.end(), buffer_remain_size, offset_, read_size))) {
        LOG_WARN("fail to do pread", KR(ret));
      } else if (read_size == 0) {
        is_read_end_ = true;
        ret = OB_ITER_END;
      } else {
        buffer.produce(read_size);
        char *cur = buffer.begin();
        while(*cur != '\n') {
          cur++;
        }
        int cur_off = cur - buffer.begin();
        buffer.consume(cur_off + 1);
        off = offset_ + cur_off + 1;
      }
    } else {
      const int64_t buffer_remain_size = buffer.get_remain_size() > (1LL << 11) ? (1LL << 11) : buffer.get_remain_size();
      int64_t read_size = 0;
      if (OB_FAIL(file_reader_.pread(buffer.end(), buffer_remain_size, offset_, read_size))) {
        LOG_WARN("fail to do pread", KR(ret));
      } else if (read_size == 0) {
        is_read_end_ = true;
        ret = OB_ITER_END;
      } else {
        buffer.produce(read_size);
        char *cur = buffer.begin();
        while(*cur != '\n') {
          cur++;
        }
        int cur_off = cur - buffer.begin();
        buffer.consume(cur_off + 1);
        off = offset_ + cur_off + 1;
      }
    }
    
  }
  return ret;
}


/**
 * ObLoadCSVPaser
 */

ObLoadCSVPaser::ObLoadCSVPaser()
  : allocator_(ObModIds::OB_SQL_LOAD_DATA), collation_type_(CS_TYPE_INVALID), mutex_(), producer_cond_(mutex_), consumer_cond_(mutex_), consumer_num_(0), no_more_data_(false), started_(false), is_inited_(false)
{
}

ObLoadCSVPaser::~ObLoadCSVPaser()
{
  reset();
}

void ObLoadCSVPaser::reset()
{
 allocator_.reset();
 collation_type_ = CS_TYPE_INVALID;
 row_.reset();
 err_records_.reset();
 is_inited_ = false;
}

int ObLoadCSVPaser::init(const ObDataInFileStruct &format, int64_t column_count,
                         ObCollationType collation_type)
{
  int ret = OB_SUCCESS;
  if (IS_INIT) {
    ret = OB_INIT_TWICE;
    LOG_WARN("ObLoadCSVPaser init twice", KR(ret), KP(this));
  } else if (OB_FAIL(csv_parser_.init(format, column_count, collation_type))) {
    LOG_WARN("fail to init csv parser", KR(ret));
  } else {
    allocator_.set_tenant_id(MTL_ID());
    ObObj *objs = nullptr;
    if (OB_ISNULL(objs = static_cast<ObObj *>(allocator_.alloc(sizeof(ObObj) * column_count)))) {
      ret = OB_ALLOCATE_MEMORY_FAILED;
      LOG_WARN("fail to alloc memory", KR(ret));
    } else {
    //  for(int i = 0; i < kConsumerNum; i++) {
      new (objs) ObObj[column_count];
      row_.cells_ = objs;
      row_.count_ = column_count;
      collation_type_ = collation_type;
      is_inited_ = true;
    //  }
    }
  }
  return ret;
}

// int ObLoadCSVPaser::get_next_row(ObLoadDataBuffer &buffer, const ObNewRow *&row, int cur_t)
// {
//   NcreMutexGuard guard(mutex_);
//   int ret = OB_SUCCESS;
//   row = nullptr;
//   if (buffer.empty()) {
//     ret = OB_ITER_END;
//   } else {
//     const char *str = buffer.begin();
//     const char *end = buffer.end();
//     int64_t nrows = 1;
//     if (OB_FAIL(csv_parser_.scan(str, end, nrows, nullptr, nullptr, unused_row_handler_,
//                                  err_records_, false))) {
//       LOG_WARN("fail to scan buffer", KR(ret));
//     } else if (OB_UNLIKELY(!err_records_.empty())) {
//       ret = err_records_.at(0).err_code;
//       LOG_WARN("fail to parse line", KR(ret));
//     } else if (0 == nrows) {
//       ret = OB_ITER_END;
//     } else {
//       buffer.consume(str - buffer.begin());
//       const ObIArray<ObCSVGeneralParser::FieldValue> &field_values_in_file =
//         csv_parser_.get_fields_per_line();
//       for (int64_t i = 0; i < row_[cur_t].count_; ++i) {
//         const ObCSVGeneralParser::FieldValue &str_v = field_values_in_file.at(i);
//         ObObj &obj = row_[cur_t].cells_[i];
//         if (str_v.is_null_) {
//           obj.set_null();
//         } else {
//           obj.set_string(ObVarcharType, ObString(str_v.len_, str_v.ptr_));
//           obj.set_collation_type(collation_type_);
//         }
//       }
//       row = &row_[cur_t];
//     }
//   }
//   return ret;
// }

int ObLoadCSVPaser::get_next_row(ObLoadDataBuffer &buffer, const ObNewRow *&row)
{
  int ret = OB_SUCCESS;
  row = nullptr;
  if (buffer.empty()) {
    ret = OB_ITER_END;
  } else {
    const char *str = buffer.begin();
    const char *end = buffer.end();
    int64_t nrows = 1;
    const char *cur = str;
    const char *cur_start = str;
    int count = 0;
    int len = 0;
    while(*cur != '\n' && cur < end) {
      if(*cur == '|') {
        ObObj &obj = row_.cells_[count];
        len = cur - cur_start;
        if (len == 4 && 0 == strncasecmp(cur_start, "NULL", 4)) {
          obj.set_null();
        } else {
          obj.set_string(ObVarcharType, ObString(len, cur_start));
          obj.set_collation_type(collation_type_);
        }
        cur_start = cur + 1;
        count++;
      }
      cur++;
    }
    if(cur >= end && *cur != '\n') {
      ret = OB_ITER_END;
    } else {
      buffer.consume(cur - str + 1);
      row = &row_;
    }
  }
  return ret;
}

void ObLoadCSVPaser::wait_by_producer(int32_t consumer_num)
{
 ncre::NcreMutexGuard guard(mutex_);
 started_ = true;
//  LOG_WARN("producer notify all consumer");
 consumer_cond_.broadcast();
 producer_cond_.wait();
 while (consumer_num_ < consumer_num) {
  // LOG_WARN("producer begin to wait", K(consumer_num_));
  producer_cond_.wait();
 }
}

void ObLoadCSVPaser::claim_no_more_data()
{
 ncre::NcreMutexGuard guard(mutex_);
//  LOG_WARN("producer claim no more data");
 no_more_data_ = true;
//  LOG_WARN("producer notify all consumer");
 consumer_cond_.broadcast();
}

bool ObLoadCSVPaser::wait_by_consumer1(int32_t consumer_num)
{
 ncre::NcreMutexGuard guard(mutex_);
 if (no_more_data_) return false;
 consumer_num_++;
 if (!started_) {
  // LOG_WARN("consumer begin wait1", K(consumer_num_));
  consumer_cond_.wait();
  // LOG_WARN("consumer finish wait1", K(consumer_num_ - 1));
 } else {
  consumer_cond_.broadcast();
 }
 consumer_num_--;
 return !no_more_data_;
}

bool ObLoadCSVPaser::wait_by_consumer2()
{
//  LOG_WARN("CCCCCCCC");
 ncre::NcreMutexGuard guard(mutex_);
//  LOG_WARN("DDDDDDDD", K(no_more_data_));
 if (no_more_data_) return false;
 consumer_num_++;
 producer_cond_.signal();
//  LOG_WARN("consumer begin wait2", K(consumer_num_));
 consumer_cond_.wait();
 consumer_num_--;
//  LOG_WARN("consumer finish wait2", K(consumer_num_));
 return !no_more_data_;
}

/**
 * ObLoadDatumRow
 */

ObLoadDatumRow::ObLoadDatumRow()
  : allocator_(ObModIds::OB_SQL_LOAD_DATA), capacity_(0), count_(0), datums_(nullptr)
{
}

ObLoadDatumRow::~ObLoadDatumRow()
{
}

void ObLoadDatumRow::reset()
{
  allocator_.reset();
  capacity_ = 0;
  count_ = 0;
  datums_ = nullptr;
}

int ObLoadDatumRow::init(int64_t capacity)
{
  int ret = OB_SUCCESS;
  if (OB_UNLIKELY(capacity <= 0)) {
    ret = OB_INVALID_ARGUMENT;
    LOG_WARN("invalid args", KR(ret), K(capacity));
  } else {
    reset();
    allocator_.set_tenant_id(MTL_ID());
    if (OB_ISNULL(datums_ = static_cast<ObStorageDatum *>(
                    allocator_.alloc(sizeof(ObStorageDatum) * capacity)))) {
      ret = OB_ALLOCATE_MEMORY_FAILED;
      LOG_WARN("fail to alloc memory", KR(ret));
    } else {
      new (datums_) ObStorageDatum[capacity];
      capacity_ = capacity;
      count_ = capacity;
    }
  }
  return ret;
}

int64_t ObLoadDatumRow::get_deep_copy_size() const
{
  int64_t size = 0;
  size += sizeof(ObStorageDatum) * count_;
  for (int64_t i = 0; i < count_; ++i) {
    size += datums_[i].get_deep_copy_size();
  }
  return size;
}

int ObLoadDatumRow::deep_copy(const ObLoadDatumRow &src, char *buf, int64_t len, int64_t &pos)
{
  int ret = OB_SUCCESS;
  if (OB_UNLIKELY(!src.is_valid())) {
    ret = OB_INVALID_ARGUMENT;
    LOG_WARN("invalid args", KR(ret), K(src));
  } else {
    reset();
    ObStorageDatum *datums = nullptr;
    const int64_t datum_cnt = src.count_;
    datums = new (buf + pos) ObStorageDatum[datum_cnt];
    pos += sizeof(ObStorageDatum) * datum_cnt;
    for (int64_t i = 0; OB_SUCC(ret) && i < datum_cnt; ++i) {
      if (OB_FAIL(datums[i].deep_copy(src.datums_[i], buf, len, pos))) {
        LOG_WARN("fail to deep copy storage datum", KR(ret), K(src.datums_[i]));
      }
    }
    if (OB_SUCC(ret)) {
      capacity_ = datum_cnt;
      count_ = datum_cnt;
      datums_ = datums;
    }
  }
  return ret;
}

DEF_TO_STRING(ObLoadDatumRow)
{
  int64_t pos = 0;
  J_OBJ_START();
  J_KV(K_(capacity), K_(count));
  if (nullptr != datums_) {
    J_ARRAY_START();
    for (int64_t i = 0; i < count_; ++i) {
      databuff_printf(buf, buf_len, pos, "col_id=%ld:", i);
      pos += datums_[i].storage_to_string(buf + pos, buf_len - pos);
      databuff_printf(buf, buf_len, pos, ",");
    }
    J_ARRAY_END();
  }
  J_OBJ_END();
  return pos;
}

OB_DEF_SERIALIZE(ObLoadDatumRow)
{
  int ret = OB_SUCCESS;
  OB_UNIS_ENCODE_ARRAY(datums_, count_);
  return ret;
}

OB_DEF_DESERIALIZE(ObLoadDatumRow)
{
  int ret = OB_SUCCESS;
  int64_t count = 0;
  OB_UNIS_DECODE(count);
  if (OB_SUCC(ret)) {
    if (OB_UNLIKELY(count <= 0)) {
      ret = OB_ERR_UNEXPECTED;
      LOG_WARN("unexpected count", K(count));
    } else if (count > capacity_ && OB_FAIL(init(count))) {
      LOG_WARN("fail to init", KR(ret));
    } else {
      OB_UNIS_DECODE_ARRAY(datums_, count);
      count_ = count;
    }
  }
  return ret;
}

OB_DEF_SERIALIZE_SIZE(ObLoadDatumRow)
{
  int64_t len = 0;
  OB_UNIS_ADD_LEN_ARRAY(datums_, count_);
  return len;
}

/**
 * ObLoadDatumRowCompare
 */

ObLoadDatumRowCompare::ObLoadDatumRowCompare()
  : result_code_(OB_SUCCESS), rowkey_column_num_(0), datum_utils_(nullptr), is_inited_(false)
{
}

ObLoadDatumRowCompare::~ObLoadDatumRowCompare()
{
}

int ObLoadDatumRowCompare::init(int64_t rowkey_column_num, const ObStorageDatumUtils *datum_utils)
{
  int ret = OB_SUCCESS;
  if (IS_INIT) {
    ret = OB_INIT_TWICE;
    LOG_WARN("ObLoadDatumRowCompare init twice", KR(ret), KP(this));
  } else if (OB_UNLIKELY(rowkey_column_num <= 0 || nullptr == datum_utils)) {
    ret = OB_INVALID_ARGUMENT;
    LOG_WARN("invalid args", KR(ret), K(rowkey_column_num), KP(datum_utils));
  } else {
    rowkey_column_num_ = rowkey_column_num;
    datum_utils_ = datum_utils;
    is_inited_ = true;
  }
  return ret;
}

bool ObLoadDatumRowCompare::operator()(const ObLoadDatumRow *lhs, const ObLoadDatumRow *rhs)
{
  int ret = OB_SUCCESS;
  int cmp_ret = 0;
  blocksstable::ObDatumRowkey lhs_rowkey_;
  blocksstable::ObDatumRowkey rhs_rowkey_;
  // if (IS_NOT_INIT) {
  //   ret = OB_NOT_INIT;
  //   LOG_WARN("ObDirectLoadDatumRowCompare not init", KR(ret), KP(this));
  // } else if (OB_ISNULL(lhs) || OB_ISNULL(rhs) ||
  //            OB_UNLIKELY(!lhs->is_valid() || !rhs->is_valid())) {
  //   ret = OB_INVALID_ARGUMENT;
  //   LOG_WARN("invalid args", KR(ret), KPC(lhs), KPC(rhs));
  // } else if (OB_UNLIKELY(lhs->count_ < rowkey_column_num_ || rhs->count_ < rowkey_column_num_)) {
  //   ret = OB_ERR_UNEXPECTED;
  //   LOG_WARN("unexpected column count", KR(ret), KPC(lhs), KPC(rhs), K_(rowkey_column_num));
  // } else {
    if (OB_FAIL(lhs_rowkey_.assign(lhs->datums_, rowkey_column_num_))) {
      LOG_WARN("fail to assign datum rowkey", KR(ret), K(lhs), K_(rowkey_column_num));
    } else if (OB_FAIL(rhs_rowkey_.assign(rhs->datums_, rowkey_column_num_))) {
      LOG_WARN("fail to assign datum rowkey", KR(ret), K(rhs), K_(rowkey_column_num));
    } else if (OB_FAIL(lhs_rowkey_.compare(rhs_rowkey_, *datum_utils_, cmp_ret))) {
      LOG_WARN("fail to compare rowkey", KR(ret), K(rhs_rowkey_), K(rhs_rowkey_), KP(datum_utils_));
    }
  // }
  // if (OB_FAIL(ret)) {
  //   result_code_ = ret;
  // }
  return cmp_ret < 0;
}



/**
 * ObLoadRowCaster
 */

ObLoadRowCaster::ObLoadRowCaster()
  : column_count_(0),
    collation_type_(CS_TYPE_INVALID),
    cast_allocator_(ObModIds::OB_SQL_LOAD_DATA),
    is_inited_(false)
{
}

ObLoadRowCaster::~ObLoadRowCaster()
{
}

int ObLoadRowCaster::init(const ObTableSchema *table_schema,
                          const ObIArray<ObLoadDataStmt::FieldOrVarStruct> &field_or_var_list)
{
  int ret = OB_SUCCESS;
  if (IS_INIT) {
    ret = OB_INIT_TWICE;
    LOG_WARN("ObLoadRowCaster init twice", KR(ret));
  } else if (OB_UNLIKELY(nullptr == table_schema || field_or_var_list.empty())) {
    ret = OB_INVALID_ARGUMENT;
    LOG_WARN("invalid args", KR(ret), KP(table_schema), K(field_or_var_list));
  } else if (OB_FAIL(OTTZ_MGR.get_tenant_tz(MTL_ID(), tz_info_.get_tz_map_wrap()))) {
    LOG_WARN("fail to get tenant time zone", KR(ret));
  } else if (OB_FAIL(init_column_schemas_and_idxs(table_schema, field_or_var_list))) {
    LOG_WARN("fail to init column schemas and idxs", KR(ret));
  } else if (OB_FAIL(datum_row_.init(table_schema->get_column_count()))) {
    LOG_WARN("fail to init datum row", KR(ret));
  } else {
    column_count_ = table_schema->get_column_count();
    collation_type_ = table_schema->get_collation_type();
    cast_allocator_.set_tenant_id(MTL_ID());
    is_inited_ = true;
  }
  return ret;
}

int ObLoadRowCaster::init_column_schemas_and_idxs(
  const ObTableSchema *table_schema,
  const ObIArray<ObLoadDataStmt::FieldOrVarStruct> &field_or_var_list)
{
  int ret = OB_SUCCESS;
  ObSEArray<ObColDesc, 64> column_descs;
  if (OB_FAIL(table_schema->get_column_ids(column_descs))) {
    LOG_WARN("fail to get column descs", KR(ret), KPC(table_schema));
  } else {
    bool found_column = true;
    for (int64_t i = 0; OB_SUCC(ret) && OB_LIKELY(found_column) && i < column_descs.count(); ++i) {
      const ObColDesc &col_desc = column_descs.at(i);
      const ObColumnSchemaV2 *col_schema = table_schema->get_column_schema(col_desc.col_id_);
      if (OB_ISNULL(col_schema)) {
        ret = OB_ERR_UNEXPECTED;
        LOG_WARN("unexpected null column schema", KR(ret), K(col_desc));
      } else if (OB_UNLIKELY(col_schema->is_hidden())) {
        ret = OB_ERR_UNEXPECTED;
        LOG_WARN("unexpected hidden column", KR(ret), K(i), KPC(col_schema));
      } else if (OB_FAIL(column_schemas_.push_back(col_schema))) {
        LOG_WARN("fail to push back column schema", KR(ret));
      } else {
        found_column = false;
      }
      // find column in source data columns
      for (int64_t j = 0; OB_SUCC(ret) && OB_LIKELY(!found_column) && j < field_or_var_list.count();
           ++j) {
        const ObLoadDataStmt::FieldOrVarStruct &field_or_var_struct = field_or_var_list.at(j);
        if (col_desc.col_id_ == field_or_var_struct.column_id_) {
          found_column = true;
          if (OB_FAIL(column_idxs_.push_back(j))) {
            LOG_WARN("fail to push back column idx", KR(ret), K(column_idxs_), K(i), K(col_desc),
                     K(j), K(field_or_var_struct));
          }
        }
      }
    }
    if (OB_SUCC(ret) && OB_UNLIKELY(!found_column)) {
      ret = OB_NOT_SUPPORTED;
      LOG_WARN("not supported incomplete column data", KR(ret), K(column_idxs_), K(column_descs),
               K(field_or_var_list));
    }
  }
  return ret;
}

int ObLoadRowCaster::get_casted_row(const ObNewRow &new_row, const ObLoadDatumRow *&datum_row)
{
  int ret = OB_SUCCESS;
  if (IS_NOT_INIT) {
    ret = OB_NOT_INIT;
    LOG_WARN("ObLoadRowCaster not init", KR(ret));
  } else {
    cast_allocator_.reuse();
    for (int64_t i = 0; OB_SUCC(ret) && i < column_idxs_.count(); ++i) {
      int64_t column_idx = column_idxs_.at(i);
      if (OB_UNLIKELY(column_idx < 0 || column_idx >= new_row.count_)) {
        ret = OB_ERR_UNEXPECTED;
        LOG_WARN("unexpected column idx", KR(ret), K(column_idx), K(new_row.count_));
      } else {
        const ObColumnSchemaV2 *column_schema = column_schemas_.at(i);
        const ObObj &src_obj = new_row.cells_[column_idx];
        ObStorageDatum &dest_datum = datum_row_.datums_[i];
        if (OB_FAIL(cast_obj_to_datum(column_schema, src_obj, dest_datum))) {
          LOG_WARN("fail to cast obj to datum", KR(ret), K(src_obj));
        }
      }
    }
    if (OB_SUCC(ret)) {
      datum_row = &datum_row_;
    }
  }
  return ret;
}

int ObLoadRowCaster::cast_obj_to_datum(const ObColumnSchemaV2 *column_schema, const ObObj &obj,
                                       ObStorageDatum &datum)
{
  int ret = OB_SUCCESS;
  ObDataTypeCastParams cast_params(&tz_info_);
  ObCastCtx cast_ctx(&cast_allocator_, &cast_params, CM_NONE, collation_type_);
  const ObObjType expect_type = column_schema->get_meta_type().get_type();
  ObObj casted_obj;
  if (obj.is_null()) {
    casted_obj.set_null();
  } else if (is_oracle_mode() && (obj.is_null_oracle() || 0 == obj.get_val_len())) {
    casted_obj.set_null();
  } else if (is_mysql_mode() && 0 == obj.get_val_len() && !ob_is_string_tc(expect_type)) {
    ObObj zero_obj;
    zero_obj.set_int(0);
    if (OB_FAIL(ObObjCaster::to_type(expect_type, cast_ctx, zero_obj, casted_obj))) {
      LOG_WARN("fail to do to type", KR(ret), K(zero_obj), K(expect_type));
    }
  } else {
    if (OB_FAIL(ObObjCaster::to_type(expect_type, cast_ctx, obj, casted_obj))) {
      LOG_WARN("fail to do to type", KR(ret), K(obj), K(expect_type));
    }
  }
  if (OB_SUCC(ret)) {
    if (OB_FAIL(datum.from_obj_enhance(casted_obj))) {
      LOG_WARN("fail to from obj enhance", KR(ret), K(casted_obj));
    }
  }
  return ret;
}

/**
 * ObLoadExternalSort
 */

ObLoadExternalSort::ObLoadExternalSort()
  : allocator_(ObModIds::OB_SQL_LOAD_DATA), is_closed_(false), is_inited_(false)
{
}

ObLoadExternalSort::~ObLoadExternalSort()
{
  external_sort_.clean_up();
}

int ObLoadExternalSort::init(const ObTableSchema *table_schema, int64_t mem_size,
                             int64_t file_buf_size)
{
  int ret = OB_SUCCESS;
  if (IS_INIT) {
    ret = OB_INIT_TWICE;
    LOG_WARN("ObLoadExternalSort init twice", KR(ret), KP(this));
  } else if (OB_UNLIKELY(nullptr == table_schema)) {
    ret = OB_INVALID_ARGUMENT;
    LOG_WARN("invalid args", KR(ret), KP(table_schema));
  } else {
    allocator_.set_tenant_id(MTL_ID());
    const int64_t rowkey_column_num = table_schema->get_rowkey_column_num();
    ObArray<ObColDesc> multi_version_column_descs;
    if (OB_FAIL(table_schema->get_multi_version_column_descs(multi_version_column_descs))) {
      LOG_WARN("fail to get multi version column descs", KR(ret));
    } else if (OB_FAIL(datum_utils_.init(multi_version_column_descs, rowkey_column_num,
                                         is_oracle_mode(), allocator_))) {
      LOG_WARN("fail to init datum utils", KR(ret));
    } else if (OB_FAIL(compare_.init(rowkey_column_num, &datum_utils_))) {
      LOG_WARN("fail to init compare", KR(ret));
    } else if (OB_FAIL(external_sort_.ncre_init(mem_size, file_buf_size, 0, MTL_ID(), &compare_, kConsumerNum))) {
      LOG_WARN("fail to init external sort", KR(ret));
    } else {
      is_inited_ = true;
    }
  }
  return ret;
}

int ObLoadExternalSort::append_row(const ObLoadDatumRow &datum_row)
{
//  ncre::NcreMutexGuard guard(mutex_);
 int ret = OB_SUCCESS;
 if (IS_NOT_INIT) {
  ret = OB_NOT_INIT;
  LOG_WARN("ObLoadExternalSort not init", KR(ret), KP(this));
 } else if (OB_UNLIKELY(is_closed_)) {
  ret = OB_ERR_UNEXPECTED;
  LOG_WARN("unexpected closed external sort", KR(ret));
 } else if (OB_FAIL(external_sort_.ncre_add_item(datum_row))) {
  LOG_WARN("fail to add item", KR(ret));
 }
 return ret;
}

int ObLoadExternalSort::close()
{
  int ret = OB_SUCCESS;
  if (IS_NOT_INIT) {
    ret = OB_NOT_INIT;
    LOG_WARN("ObLoadExternalSort not init", KR(ret), KP(this));
  } else if (OB_UNLIKELY(is_closed_)) {
    ret = OB_ERR_UNEXPECTED;
    LOG_WARN("unexpected closed external sort", KR(ret));
  } else if (OB_FAIL(external_sort_.do_sort(true))) {
    LOG_WARN("fail to do sort", KR(ret));
  } else {
    is_closed_ = true;
  }
  return ret;
}

int ObLoadExternalSort::get_next_row(const ObLoadDatumRow *&datum_row)
{
  int ret = OB_SUCCESS;
  if (IS_NOT_INIT) {
    ret = OB_NOT_INIT;
    LOG_WARN("ObLoadExternalSort not init", KR(ret), KP(this));
  } else if (OB_UNLIKELY(!is_closed_)) {
    ret = OB_ERR_UNEXPECTED;
    LOG_WARN("unexpected not closed external sort", KR(ret));
  } else if (OB_FAIL(external_sort_.get_next_item(datum_row))) {
    LOG_WARN("fail to get next item", KR(ret));
  }
  return ret;
}

/**
 * ObLoadSSTableWriter
 */

ObLoadSSTableWriter::ObLoadSSTableWriter()
  : rowkey_column_num_(0),
    extra_rowkey_column_num_(0),
    column_count_(0),
    is_closed_(false),
    is_inited_(false)
{
}

ObLoadSSTableWriter::~ObLoadSSTableWriter()
{
}

int ObLoadSSTableWriter::init(const ObTableSchema *table_schema)
{
  int ret = OB_SUCCESS;
  if (IS_INIT) {
    ret = OB_INIT_TWICE;
    LOG_WARN("ObLoadSSTableWriter init twice", KR(ret), KP(this));
  } else if (OB_UNLIKELY(nullptr == table_schema)) {
    ret = OB_INVALID_ARGUMENT;
    LOG_WARN("invalid args", KR(ret), KP(table_schema));
  } else {
    tablet_id_ = table_schema->get_tablet_id();
    rowkey_column_num_ = table_schema->get_rowkey_column_num();
    extra_rowkey_column_num_ = ObMultiVersionRowkeyHelpper::get_extra_rowkey_col_cnt();
    column_count_ = table_schema->get_column_count();
    ObLocationService *location_service = nullptr;
    bool is_cache_hit = false;
    ObLSService *ls_service = nullptr;
    ObLS *ls = nullptr;
    if (OB_ISNULL(location_service = GCTX.location_service_)) {
      ret = OB_ERR_SYS;
      LOG_WARN("location service is null", KR(ret), KP(location_service));
    } else if (OB_FAIL(
                 location_service->get(MTL_ID(), tablet_id_, INT64_MAX, is_cache_hit, ls_id_))) {
      LOG_WARN("fail to get ls id", KR(ret), K(tablet_id_));
    } else if (OB_ISNULL(ls_service = MTL(ObLSService *))) {
      ret = OB_ERR_SYS;
      LOG_ERROR("ls service is null", KR(ret));
    } else if (OB_FAIL(ls_service->get_ls(ls_id_, ls_handle_, ObLSGetMod::STORAGE_MOD))) {
      LOG_WARN("fail to get ls", KR(ret), K(ls_id_));
    } else if (OB_ISNULL(ls = ls_handle_.get_ls())) {
      ret = OB_ERR_UNEXPECTED;
      LOG_ERROR("ls should not be null", KR(ret));
    } else if (OB_FAIL(ls->get_tablet(tablet_id_, tablet_handle_))) {
      LOG_WARN("fail to get tablet handle", KR(ret), K(tablet_id_));
    } else if (OB_FAIL(init_sstable_index_builder(table_schema))) {
      LOG_WARN("fail to init sstable index builder", KR(ret));
    } else if (OB_FAIL(init_macro_block_writer(table_schema))) {
      LOG_WARN("fail to init macro block writer", KR(ret));
    } else if (OB_FAIL(datum_row_.init(column_count_ + extra_rowkey_column_num_))) {
      LOG_WARN("fail to init datum row", KR(ret));
    } else {
      // free_queue_.init(MaxMacroWriterNum * 3);
      // full_queue_.init(MaxMacroWriterNum * 3);
      // for(int i = 0; i < kConsumerNumSstable + AddAllocate; i++) {
      //   int_vec_[i] = i;
      //   full_queue_.push(&int_vec_[i]);
      // }
      // for(int i = 0; i < MaxMacroWriterNum; i++) {
      //   macro_count_vec_[i] = i;
      // }
      // for(int i = 0; i < kConsumerNumSstable; i++) {
      //   priority_queue_[i].init(MaxMacroBlockNum);
      //   close_queue_[i].init(MaxMacroWriterNum);
      // }
      // int_vec_[kConsumerNumSstable + AddAllocate] = -1;
      table_key_.table_type_ = ObITable::MAJOR_SSTABLE;
      table_key_.tablet_id_ = tablet_id_;
      table_key_.log_ts_range_.start_log_ts_ = 0;
      table_key_.log_ts_range_.end_log_ts_ = ObTimeUtil::current_time_ns();
      datum_row_.row_flag_.set_flag(ObDmlFlag::DF_INSERT);
      datum_row_.mvcc_row_flag_.set_last_multi_version_row(true);
      datum_row_.storage_datums_[rowkey_column_num_].set_int(-1); // fill trans_version
      datum_row_.storage_datums_[rowkey_column_num_ + 1].set_int(0); // fill sql_no

      is_inited_ = true;
    }
  }
  return ret;
}

int ObLoadSSTableWriter::init_sstable_index_builder(const ObTableSchema *table_schema)
{
  int ret = OB_SUCCESS;
  ObDataStoreDesc data_desc;
  if (OB_FAIL(data_desc.init(*table_schema, ls_id_, tablet_id_, MAJOR_MERGE, 1L))) {
    LOG_WARN("fail to init data desc", KR(ret));
  } else {
    data_desc.row_column_count_ = data_desc.rowkey_column_count_ + 1;
    data_desc.need_prebuild_bloomfilter_ = false;
    data_desc.col_desc_array_.reset();
    if (OB_FAIL(data_desc.col_desc_array_.init(data_desc.row_column_count_))) {
      LOG_WARN("fail to reserve column desc array", KR(ret));
    } else if (OB_FAIL(table_schema->get_rowkey_column_ids(data_desc.col_desc_array_))) {
      LOG_WARN("fail to get rowkey column ids", KR(ret));
    } else if (OB_FAIL(
                 ObMultiVersionRowkeyHelpper::add_extra_rowkey_cols(data_desc.col_desc_array_))) {
      LOG_WARN("fail to add extra rowkey cols", KR(ret));
    } else {
      ObObjMeta meta;
      meta.set_varchar();
      meta.set_collation_type(CS_TYPE_BINARY);
      ObColDesc col;
      col.col_id_ = static_cast<uint64_t>(data_desc.row_column_count_ + OB_APP_MIN_COLUMN_ID);
      col.col_type_ = meta;
      col.col_order_ = DESC;
      if (OB_FAIL(data_desc.col_desc_array_.push_back(col))) {
        LOG_WARN("fail to push back last col for index", KR(ret), K(col));
      }
    }
  }
  if (OB_SUCC(ret)) {
    if (OB_FAIL(sstable_index_builder_.init(data_desc))) {
      LOG_WARN("fail to init index builder", KR(ret), K(data_desc));
    }
  }
  return ret;
}

int ObLoadSSTableWriter::init_macro_block_writer(const ObTableSchema *table_schema)
{
  int ret = OB_SUCCESS;
  if (OB_FAIL(data_store_desc_.init(*table_schema, ls_id_, tablet_id_, MAJOR_MERGE, 1))) {
    LOG_WARN("fail to init data_store_desc", KR(ret), K(tablet_id_));
  } else {
    data_store_desc_.sstable_index_builder_ = &sstable_index_builder_;
  }
  // if (OB_SUCC(ret)) {
    // for(int64_t i = 0; i < MaxMacroWriterNum; i++) {
    //   ObMacroDataSeq data_seq(i << 30);
    //   data_seq.set_parallel_degree(i);
    //   if (OB_FAIL(macro_block_writer_[i].open(data_store_desc_, data_seq))) {
    //     LOG_WARN("fail to init macro block writer", KR(ret), K(data_store_desc_), K(data_seq));
    //   }
    // }
  // }
  return ret;
}

// int ObLoadSSTableWriter::append_row(const ObLoadDatumRow &datum_row, int cur)
// {
//   int ret = OB_SUCCESS;
//   if (IS_NOT_INIT) {
//     ret = OB_NOT_INIT;
//     LOG_WARN("ObLoadSSTableWriter not init", KR(ret), KP(this));
//   } else if (OB_UNLIKELY(is_closed_)) {
//     ret = OB_ERR_UNEXPECTED;
//     LOG_WARN("unexpected closed external sort", KR(ret));
//   } else if (OB_UNLIKELY(!datum_row.is_valid() || datum_row.count_ != column_count_)) {
//     ret = OB_INVALID_ARGUMENT;
//     LOG_WARN("invalid args", KR(ret), K(datum_row), K(column_count_));
//   } else {
//     ObLoadDatumRow *new_item = NULL;
//     const int64_t item_size = sizeof(ObLoadDatumRow) + datum_row.get_deep_copy_size();
//     char *buf = NULL;
//     if(allocator_[cur].used() + item_size > MacroAlloacatorSize) {
//       return OB_ITER_END;
//     } else if (OB_ISNULL(buf = static_cast<char *>(allocator_[cur].alloc(item_size)))) {
//       ret = common::OB_ALLOCATE_MEMORY_FAILED;
//       STORAGE_LOG(WARN, "fail to allocate memory", K(ret), K(item_size));
//     } else if (OB_ISNULL(new_item = new (buf) ObLoadDatumRow())) {
//       ret = common::OB_ALLOCATE_MEMORY_FAILED;
//       STORAGE_LOG(WARN, "fail to placement new item", K(ret));
//     }  else {
//       int64_t buf_pos = sizeof(ObLoadDatumRow);
//       if (OB_FAIL(new_item->deep_copy(datum_row, buf, item_size, buf_pos))) {
//         STORAGE_LOG(WARN, "fail to deep copy item", K(ret));
//       } else if (OB_FAIL(item_list_[cur].push_back(new_item))) {
//         STORAGE_LOG(WARN, "fail to push back new item", K(ret));
//       }
//     }
    // for (int64_t i = 0; i < column_count_; ++i) {
    //   if (i < rowkey_column_num_) {
    //     datum_row_.storage_datums_[i] = datum_row.datums_[i];
    //   } else {
    //     datum_row_.storage_datums_[i + extra_rowkey_column_num_] = datum_row.datums_[i];
    //   }
    // }

    // if (OB_FAIL(macro_block_writer_.append_row(datum_row_))) {
    //   LOG_WARN("fail to append row", KR(ret));
    // }
//   }

//   return ret;
// }

int ObLoadSSTableWriter::create_sstable()
{
  int ret = OB_SUCCESS;
  ObTableHandleV2 table_handle;
  SMART_VAR(ObSSTableMergeRes, merge_res)
  {
    const ObStorageSchema &storage_schema = tablet_handle_.get_obj()->get_storage_schema();
    int64_t column_count = 0;
    if (OB_FAIL(storage_schema.get_stored_column_count_in_sstable(column_count))) {
      LOG_WARN("fail to get stored column count in sstable", KR(ret));
    } else if (OB_FAIL(sstable_index_builder_.close(column_count, merge_res))) {
      LOG_WARN("fail to close sstable index builder", KR(ret));
    } else {
      ObTabletCreateSSTableParam create_param;
      create_param.table_key_ = table_key_;
      create_param.table_mode_ = storage_schema.get_table_mode_struct();
      create_param.index_type_ = storage_schema.get_index_type();
      create_param.rowkey_column_cnt_ = storage_schema.get_rowkey_column_num() +
                                        ObMultiVersionRowkeyHelpper::get_extra_rowkey_col_cnt();
      create_param.schema_version_ = storage_schema.get_schema_version();
      create_param.create_snapshot_version_ = 0;
      ObSSTableMergeRes::fill_addr_and_data(merge_res.root_desc_, create_param.root_block_addr_,
                                            create_param.root_block_data_);
      ObSSTableMergeRes::fill_addr_and_data(merge_res.data_root_desc_,
                                            create_param.data_block_macro_meta_addr_,
                                            create_param.data_block_macro_meta_);
      create_param.root_row_store_type_ = merge_res.root_desc_.row_type_;
      create_param.data_index_tree_height_ = merge_res.root_desc_.height_;
      create_param.index_blocks_cnt_ = merge_res.index_blocks_cnt_;
      create_param.data_blocks_cnt_ = merge_res.data_blocks_cnt_;
      create_param.micro_block_cnt_ = merge_res.micro_block_cnt_;
      create_param.use_old_macro_block_count_ = merge_res.use_old_macro_block_count_;
      create_param.row_count_ = merge_res.row_count_;
      create_param.column_cnt_ = merge_res.data_column_cnt_;
      create_param.data_checksum_ = merge_res.data_checksum_;
      create_param.occupy_size_ = merge_res.occupy_size_;
      create_param.original_size_ = merge_res.original_size_;
      create_param.max_merged_trans_version_ = merge_res.max_merged_trans_version_;
      create_param.contain_uncommitted_row_ = merge_res.contain_uncommitted_row_;
      create_param.compressor_type_ = merge_res.compressor_type_;
      create_param.encrypt_id_ = merge_res.encrypt_id_;
      create_param.master_key_id_ = merge_res.master_key_id_;
      create_param.data_block_ids_ = merge_res.data_block_ids_;
      create_param.other_block_ids_ = merge_res.other_block_ids_;
      MEMCPY(create_param.encrypt_key_, merge_res.encrypt_key_,
             OB_MAX_TABLESPACE_ENCRYPT_KEY_LENGTH);
      if (OB_FAIL(
            merge_res.fill_column_checksum(&storage_schema, create_param.column_checksums_))) {
        LOG_WARN("fail to fill column checksum for empty major", KR(ret), K(create_param));
      } else if (OB_FAIL(ObTabletCreateDeleteHelper::create_sstable(create_param, table_handle))) {
        LOG_WARN("fail to create sstable", KR(ret), K(create_param));
      } else {
        const int64_t rebuild_seq = ls_handle_.get_ls()->get_rebuild_seq();
        ObTabletHandle new_tablet_handle;
        ObUpdateTableStoreParam table_store_param(table_handle,
                                                  tablet_handle_.get_obj()->get_snapshot_version(),
                                                  false, &storage_schema, rebuild_seq, true, true);
        if (OB_FAIL(ls_handle_.get_ls()->update_tablet_table_store(tablet_id_, table_store_param,
                                                                   new_tablet_handle))) {
          LOG_WARN("fail to update tablet table store", KR(ret), K(tablet_id_),
                   K(table_store_param));
        }
      }
    }
  }
  return ret;
}

int ObLoadSSTableWriter::close()
{
  int ret = OB_SUCCESS;
  if (IS_NOT_INIT) {
    ret = OB_NOT_INIT;
    LOG_WARN("ObLoadSSTableWriter not init", KR(ret), KP(this));
  } else if (OB_UNLIKELY(is_closed_)) {
    ret = OB_ERR_UNEXPECTED;
    LOG_WARN("unexpected closed sstable writer", KR(ret));
  } else {
    ObSSTable *sstable = nullptr;
    // if (OB_FAIL(macro_block_writer_.close())) {
    //   LOG_WARN("fail to close macro block writer", KR(ret));
    // } else 
    if (OB_FAIL(create_sstable())) {
      LOG_WARN("fail to create sstable", KR(ret));
    } else {
      is_closed_ = true;
    }
  }
  return ret;
}

/**
 * ObLoadDataDirectDemo
 */

ObLoadDataDirectDemo::ObLoadDataDirectDemo()
{
}

ObLoadDataDirectDemo::~ObLoadDataDirectDemo()
{
}

int ObLoadDataDirectDemo::execute(ObExecContext &ctx, ObLoadDataStmt &load_stmt)
{
  int ret = OB_SUCCESS;
  if (OB_FAIL(inner_init(load_stmt))) {
    LOG_WARN("fail to inner init", KR(ret));
  } else if (OB_FAIL(do_load())) {
    LOG_WARN("fail to do load", KR(ret));
  }
  return ret;
}

int ObLoadDataDirectDemo::inner_init(ObLoadDataStmt &load_stmt)
{
  int ret = OB_SUCCESS;
  const ObLoadArgument &load_args = load_stmt.get_load_arguments();
  const ObIArray<ObLoadDataStmt::FieldOrVarStruct> &field_or_var_list =
    load_stmt.get_field_or_var_list();
  const uint64_t tenant_id = load_args.tenant_id_;
  const uint64_t table_id = load_args.table_id_;
  ObSchemaGetterGuard schema_guard;
  const ObTableSchema *table_schema = nullptr;
  if (OB_FAIL(ObMultiVersionSchemaService::get_instance().get_tenant_schema_guard(tenant_id,
                                                                                  schema_guard))) {
    LOG_WARN("fail to get tenant schema guard", KR(ret), K(tenant_id));
  } else if (OB_FAIL(schema_guard.get_table_schema(tenant_id, table_id, table_schema))) {
    LOG_WARN("fail to get table schema", KR(ret), K(tenant_id), K(table_id));
  } else if (OB_ISNULL(table_schema)) {
    ret = OB_TABLE_NOT_EXIST;
    LOG_WARN("table not exist", KR(ret), K(tenant_id), K(table_id));
  } else if (OB_UNLIKELY(table_schema->is_heap_table())) {
    ret = OB_NOT_SUPPORTED;
    LOG_WARN("not support heap table", KR(ret));
  }
  // init csv_parser_
  else {
    for(int i = 0; i < kConsumerNum + 1; i++) {
      if (OB_FAIL(csv_parser_[i].init(load_stmt.get_data_struct_in_file(), field_or_var_list.count(),
                                        load_args.file_cs_type_))) {
        LOG_WARN("fail to init csv parser", KR(ret));
        break;
      }
      // init file_reader_
      else if (OB_FAIL(file_reader_[i].open(load_args.full_file_path_))) {
        LOG_WARN("fail to open file", KR(ret), K(load_args.full_file_path_));
        break;
      }
      // init buffer_
       else if (OB_FAIL(external_sort_[i].init(table_schema, MEM_BUFFER_SIZE, FILE_BUFFER_SIZE))) {
        LOG_WARN("fail to init row caster", KR(ret));
      }
    }
    for(int i = 0; i < kConsumerNum; i++) {
      if (OB_FAIL(buffer_[i].create(FILE_BUFFER_SIZE))) {
        LOG_WARN("fail to create buffer", KR(ret));
        break;
      }
    }
    buffer_[kConsumerNum].create((4LL << 20));
  }
  
  // init sstable_writer_
  if (OB_FAIL(sstable_writer_.init(table_schema))) {
    LOG_WARN("fail to init sstable writer", KR(ret));
  }
  // init row_caster_
  else {
    for (int32_t i = 0; i < kConsumerNum; i++) {
      row_caster[i].init(table_schema, field_or_var_list);
    }
  }
  return ret;
}

// void produce_row(ObLoadDataDirectDemo *demo)
// {
//  int ret = OB_SUCCESS;
//  const ObNewRow *new_row = nullptr;
//  const ObLoadDatumRow *datum_row = nullptr;
//  while (OB_SUCC(ret)) {
//   if (OB_FAIL(demo->buffer_.squash())) {
//    LOG_WARN("fail to squash buffer", KR(ret));
//   } else if (OB_FAIL(demo->file_reader_.read_next_buffer(demo->buffer_))) {
//    if (OB_UNLIKELY(OB_ITER_END != ret)) {
//     LOG_WARN("fail to read next buffer", KR(ret));
//    } else {
//     if (OB_UNLIKELY(!demo->buffer_.empty())) {
//      ret = OB_ERR_UNEXPECTED;
//      LOG_WARN("unexpected incomplate data", KR(ret));
//     }
//     ret = OB_SUCCESS;
//     break;
//    }
//   } else if (OB_UNLIKELY(demo->buffer_.empty())) {
//    ret = OB_ERR_UNEXPECTED;
//    LOG_WARN("unexpected empty buffer", KR(ret));
//   }
//   demo->csv_parser_.wait_by_producer(kConsumerNum);
//  }
//  demo->csv_parser_.claim_no_more_data();
// }
void produce_datum(ObLoadDataDirectDemo *demo, int cur)
{
 int ret = OB_SUCCESS;
 const ObLoadDatumRow *datum_row = nullptr;
 const ObNewRow *new_row = nullptr;
 std::pair<int64_t, int64_t> my_pair;
 void *p = &my_pair;
 int64_t start_off = 0;
 int64_t end_off = 0;
//  if (!demo->csv_parser_.wait_by_consumer1(kConsumerNum)) return;
 while(OB_SUCC(demo->cur_queue.pop(p, MaxTimeout))) {
  my_pair = *(std::pair<int64_t, int64_t> *)(p);
  start_off = my_pair.first;
  end_off = my_pair.second;
  if(start_off == -1) {
    demo->external_sort_[cur].external_sort_.wait_final(true);
    return ;
  }
  demo->file_reader_[cur].set_off(start_off);
  demo->file_reader_[cur].set_end_off(end_off);
  demo->buffer_[cur].clear();
  while (OB_SUCC(ret)) {
    if (OB_FAIL(demo->buffer_[cur].squash())) {
    LOG_WARN("fail to squash buffer", KR(ret));
    } else if (OB_FAIL(demo->file_reader_[cur].read_next_buffer(demo->buffer_[cur]))) {
    if (OB_UNLIKELY(OB_ITER_END != ret)) {
      LOG_WARN("fail to read next buffer", KR(ret));
    } else {
      if (OB_UNLIKELY(!demo->buffer_[cur].empty())) {
      ret = OB_ERR_UNEXPECTED;
      LOG_WARN("unexpected incomplate data", KR(ret));
      }
      ret = OB_SUCCESS;
      break;
    }
    } else if (OB_UNLIKELY(demo->buffer_[cur].empty())) {
    ret = OB_ERR_UNEXPECTED;
    LOG_WARN("unexpected empty buffer", KR(ret));
    } else {
      while (OB_SUCC(ret)) {
        if (OB_FAIL(demo->csv_parser_[cur].get_next_row(demo->buffer_[cur], new_row))) {
          if (OB_UNLIKELY(OB_ITER_END != ret)) {
            LOG_WARN("fail to get next row", KR(ret));
          } else {
            ret = OB_SUCCESS;
            break;
          }
        } else if (OB_FAIL(demo->row_caster[cur].get_casted_row(*new_row, datum_row))) {
          LOG_WARN("fail to cast row", KR(ret));
        } else {
          demo->external_sort_[cur].append_row(*datum_row);
        }
      }
    }
  }
 }
}

void consume_datum(ObLoadSSTableWriter &sstable_writer, int cur, ObFragmentMerge<ObLoadDatumRow, ObLoadDatumRowCompare> *merge) {
  int ret = OB_SUCCESS;
  std::pair<int,int> pair;
  void *p = &pair;
  blocksstable::ObDatumRow datum_row;
  // ObLoadDatumRow load_datum_row;
  // load_datum_row.init(sstable_writer.column_count_);
  const ObLoadDatumRow *ob_pointer = nullptr;
  datum_row.init(sstable_writer.column_count_ + sstable_writer.extra_rowkey_column_num_);
  datum_row.row_flag_.set_flag(ObDmlFlag::DF_INSERT);
  datum_row.mvcc_row_flag_.set_last_multi_version_row(true);
  datum_row.storage_datums_[sstable_writer.rowkey_column_num_].set_int(-1); // fill trans_version
  datum_row.storage_datums_[sstable_writer.rowkey_column_num_ + 1].set_int(0); // fill sql_no
  int64_t pop_start_time = common::ObTimeUtility::current_time();
  
  blocksstable::ObMacroBlockWriter macro_block_writer;
  ObMacroDataSeq data_seq;
  data_seq.set_parallel_degree(cur);
  int count = 0;
  if (OB_FAIL(macro_block_writer.open(sstable_writer.data_store_desc_, data_seq))) {
    LOG_WARN("fail to init macro block writer", KR(ret), K(sstable_writer.data_store_desc_), K(data_seq));
  }
  while(OB_SUCC(ret)) {
    if(OB_FAIL(merge->get_next_item(ob_pointer))) {
      if (OB_UNLIKELY(OB_ITER_END != ret)) {
        LOG_WARN("fail to get next row", KR(ret));
      } else {
        LOG_WARN("the thread data count",KR(ret), K(count), K(cur));
        macro_block_writer.close();
        return;
      }
    } else {
      for (int64_t i = 0; i < sstable_writer.column_count_; ++i) {
        if (i < sstable_writer.rowkey_column_num_) {
          datum_row.storage_datums_[i] = ob_pointer->datums_[i];
        } else {
          datum_row.storage_datums_[i + sstable_writer.extra_rowkey_column_num_] = ob_pointer->datums_[i];
        }
      }
      if (OB_FAIL(macro_block_writer.append_row(datum_row))) {
        LOG_WARN("fail to append row", KR(ret), K(count));
      }
    }
    count++;
  }
  LOG_WARN("FAILED TO WRITE FRAGMENT", KR(ret));
}


// void write_macro(ObLoadSSTableWriter &sstable_writer) {
//   int ret = OB_SUCCESS;
//   void *p = nullptr;
//   ObMacroBlockHandle *handler = nullptr;
//   while(sstable_writer.open_ == true) {
//     for(int i = 0; i < kConsumerNumSstable; i++) {
//       if(sstable_writer.priority_queue_[i].size() == 0) {
//         continue;
//       } else {
//         sstable_writer.priority_queue_[i].pop(p,MaxTimeout);
//         handler = (ObMacroBlockHandle *)p;
//         if(handler->is_final_) {
//           sstable_writer.close_queue_[i].push(p);
//           break;
//         }
//         // LOG_WARN("START WRITE");
//         handler->ncre_write();
//         // LOG_WARN("FINISH WRITE");
//         break;
//       }
//      }
//   }
// }
static void my_merge(common::ObVector<ObLoadDatumRow *> *item_list, ObLoadDatumRowCompare *compare, int start1, int end1, int start2, int end2){
  ObVector<ObLoadDatumRow *> vec(end2 - start1);
  for(int i = start1; i < end2; i++) {
    vec.push_back(item_list->at(i));
  }

  int i = 0;
  int j = start2 - start1;
  int k = start1;
  int size1 = j;
  int size2 = end2 - start1;
  while (i < size1 && j < size2) {
    if ((*compare)(vec[i], vec[j])) {
      item_list->at(k) = vec[i];
      i++;
      k++;
    }
    else {
      item_list->at(k) = vec[j];
      j++;
      k++;
    }
  }

  while (i < size1) {
    item_list->at(k) = vec[i];
    i++;
    k++;
  }
  while (j < size2) {
    item_list->at(k) = vec[j];
    j++;
    k++;
  }
}

static void my_sort(common::ObVector<ObLoadDatumRow *> *item_list, ObLoadDatumRowCompare *compare, int begin, int end, int depth, int max_depth) {
  if (end <= begin) return;
  if(max_depth == depth) {
    std::sort(item_list->begin() + begin, item_list->begin() + end, *compare);
  } else {
    int mid = (begin + end) >> 1;
    oceanbase::lib::Thread::Runnable job1 = std::bind(my_sort, item_list, compare, begin, mid + 1, depth + 1 , max_depth);
    oceanbase::lib::Thread::Runnable job2 = std::bind(my_sort, item_list, compare, mid + 1, end, depth + 1 , max_depth);
    ncre::NcreThread thread1(job1);
    ncre::NcreThread thread2(job2);
    thread1.start();
    thread2.start();
    thread1.wait();
    thread2.wait();
    thread1.destroy();
    thread2.destroy();
    my_merge(item_list, compare, begin, mid + 1, mid + 1, end);
  }
}
int ObLoadDataDirectDemo::do_load()
{
  sstable_writer_.open_ = true;
  common::ObArenaAllocator allocator(common::ObNewModIds::OB_ASYNC_EXTERNAL_SORTER);
  char *buf = nullptr;
  ncre::NcreThread *t = nullptr;
  ObVector<ncre::NcreThread *> threads;

  const ObNewRow *new_row = nullptr;
  cur_queue.init(1024);
  int64_t off = 0;
  std::pair<int64_t, int64_t> store_sequence[1024];  
  int cur_count = 0;
  store_sequence[cur_count].first = 0;
  int ret = OB_SUCCESS;
  void *pair_p = nullptr;
  
  bool is_first = true;
  ObVector<ObLoadDatumRow *> item_list;
  ObVector<ObLoadDatumRow *> range_list; 
  ObFragmentMerge<ObLoadDatumRow, ObLoadDatumRowCompare> mergers[kConsumerNumSstable];
  for(int i = 0; i < kConsumerNumSstable; i++) {
    mergers[i].compare_.set_compare(&external_sort_[0].compare_);
    mergers[i].is_inited_ = true;
    mergers[i].is_opened_ = false;
    for(int j = 0; j < kConsumerNum + 1; j++) {
      external_sort_[j].external_sort_.memory_sort_round_.mergers_.push_back(&mergers[i]);
    }
  }
  //  LOG_WARN("START_EXTERNAL_SORT");
  while (OB_SUCC(ret)) {
    if (OB_FAIL(file_reader_[kConsumerNum].read_next_squence(buffer_[kConsumerNum], off, is_first))) {
      if (OB_UNLIKELY(OB_ITER_END != ret)) {
        LOG_WARN("fail to read next buffer", KR(ret));
      } else {
        if (OB_UNLIKELY(!buffer_[kConsumerNum].empty())) {
          ret = OB_ERR_UNEXPECTED;
          LOG_WARN("unexpected incomplate data", KR(ret));
        }
        ret = OB_SUCCESS;
        break;
      }
    } else if (OB_UNLIKELY(buffer_[kConsumerNum].empty())) {
      ret = OB_ERR_UNEXPECTED;
      LOG_WARN("unexpected empty buffer", KR(ret));
    }
    if(is_first) {
      LOG_WARN("START");
      is_first = false;
      const ObLoadDatumRow *datum_row = nullptr;
      const ObNewRow *new_row = nullptr;
      while (OB_SUCC(ret)) {
        if (OB_FAIL(csv_parser_[0].get_next_row(buffer_[kConsumerNum], new_row))) {
          if (OB_UNLIKELY(OB_ITER_END != ret)) {
            LOG_WARN("fail to get next row", KR(ret));
          } else {
            ret = OB_SUCCESS;
            break;
          }
        } else if (OB_FAIL(row_caster[0].get_casted_row(*new_row, datum_row))) {
          LOG_WARN("fail to cast row", KR(ret));
        } else {
          const int64_t item_size = sizeof(ObLoadDatumRow) + (*datum_row).get_deep_copy_size();
          char *buf = NULL;
          ObLoadDatumRow *new_item = NULL;
          if (OB_ISNULL(buf = static_cast<char *>(allocator.alloc(item_size)))) {
            ret = common::OB_ALLOCATE_MEMORY_FAILED;
            STORAGE_LOG(WARN, "fail to allocate memory", K(ret), K(item_size));
          } else if (OB_ISNULL(new_item = new (buf) ObLoadDatumRow())) {
            ret = common::OB_ALLOCATE_MEMORY_FAILED;
            STORAGE_LOG(WARN, "fail to placement new item", K(ret));
          } else {
            int64_t buf_pos = sizeof(ObLoadDatumRow);
            if (OB_FAIL(new_item->deep_copy((*datum_row), buf, item_size, buf_pos))) {
              STORAGE_LOG(WARN, "fail to deep copy item", K(ret));
            } else if (OB_FAIL(item_list.push_back(new_item))) {
              STORAGE_LOG(WARN, "fail to push back new item", K(ret));
            }
          }
        }
      }
      ret = OB_SUCCESS;
      LOG_WARN("END", K(item_list.size()));
      my_sort(&item_list, &external_sort_[0].compare_, 0, item_list.size(), 0 ,3);
      for(int i = 1; i < kConsumerNumSstable; i++) {
        range_list.push_back(item_list[int(item_list.size() / kConsumerNumSstable * i)]);
      }
      LOG_WARN("END1");
      
      for (int32_t i = 0; i < kConsumerNum; i++) {
        external_sort_[i].external_sort_.memory_sort_round_.range_list_.assign(range_list);
        Thread::Runnable job = std::bind(produce_datum, this, i);
        buf = reinterpret_cast<char *>(allocator.alloc(sizeof(ncre::NcreThread)));
        t = new (buf) ncre::NcreThread(job);
        threads.push_back(t);
      }
      for (int32_t i = 0; i < threads.size(); i++) {
        threads[i]->start();
      }
    }
    store_sequence[cur_count].second = off;
    pair_p = &store_sequence[cur_count];
    cur_queue.push(pair_p);
    cur_count++;
    store_sequence[cur_count].first = off;
  }
  store_sequence[cur_count].second = off + BigOffset;
  pair_p = &store_sequence[cur_count];
  cur_queue.push(pair_p);
  for(int i = 0; i < kConsumerNum; i++) {
    cur_count++;
    store_sequence[cur_count].first = -1;
    store_sequence[cur_count].second = -1;
    cur_queue.push(static_cast<void *>(&store_sequence[cur_count]));
  }
  //  LOG_WARN("READ_INIT_END");  
  for (int32_t i = 0; i < threads.size(); i++) {
    threads[i]->wait();
    threads[i]->destroy();
  }
  threads.clear();
  // external_sort_.external_sort_.wait_final(true);
 
  const ObLoadDatumRow *datum_row = nullptr; 
  ret = OB_SUCCESS;
  
  for (int32_t i = 0; i < kConsumerNumSstable; i++) {
    if (OB_FAIL(mergers[i].open())) {
      STORAGE_LOG(WARN, "fail to open merger", K(ret));
    }
    Thread::Runnable job = std::bind(consume_datum, std::ref(sstable_writer_), i, &mergers[i]);
    buf = reinterpret_cast<char *>(allocator.alloc(sizeof(ncre::NcreThread)));
    t = new (buf) ncre::NcreThread(job);
    threads.push_back(t);
  }
  // ncre::NcreThread write_thread(write_job);
  for (int32_t i = 0; i < threads.size(); i++) {
    threads[i]->start();
  }
  // for(int i = 1; i < kConsumerNum; i++) {
  //   external_sort_[0].external_sort_.assign_external_sort(external_sort_[i].external_sort_);
  // }
  // if (OB_SUCC(ret)) {
  //   mergers[i]
    // if (OB_FAIL(external_sort_[0].external_sort_.do_merge(true))) {
    //   LOG_WARN("fail to close external sort", KR(ret));
    // }
  //   external_sort_[0].is_closed_ = true;
  // }
//   int macro_count = 0;
//   std::pair<int, int> pairs[MaxMacroWriterNum];

//   Thread::Runnable write_job = std::bind(write_macro, std::ref(sstable_writer_));
//   for (int32_t i = 0; i < kConsumerNumSstable; i++) {
//     Thread::Runnable job = std::bind(consum_datum, std::ref(sstable_writer_), i);
//     buf = reinterpret_cast<char *>(allocator.alloc(sizeof(ncre::NcreThread)));
//     t = new (buf) ncre::NcreThread(job);
//     threads.push_back(t);
//   }
//   ncre::NcreThread write_thread(write_job);
//   for (int32_t i = 0; i < threads.size(); i++) {
//     threads[i]->start();
//   }
//   write_thread.start();
//   // LOG_WARN("START_SSTABLE_WRITE");
// while (OB_SUCC(ret)) {
//   int i = 0;
//   void *p = nullptr;
//   if(OB_FAIL(sstable_writer_.full_queue_.pop(p, MaxTimeout))) {
//     LOG_WARN("TIMEOUT ERROR");
//   }
//   i = *(int*)p;
//   while(OB_SUCC(ret)){
//     if (OB_FAIL(external_sort_[0].get_next_row(datum_row))) {
//       if (OB_UNLIKELY(OB_ITER_END != ret)) {
//         LOG_WARN("fail to get next row", KR(ret));
//       } else {
//         pairs[macro_count].first = i;
//         pairs[macro_count].second = macro_count;
//         pair_p = &pairs[macro_count];
//         macro_count++;
//         sstable_writer_.free_queue_.push(pair_p);
//         for(int j = 0; j < kConsumerNumSstable; j++) {
//           pairs[macro_count].first = -1;
//           pairs[macro_count].second = -1;
//           pair_p = &pairs[macro_count];
//           sstable_writer_.free_queue_.push(pair_p);
//           macro_count++;
//         }
//         break;
//       }
//       } else if (OB_FAIL(sstable_writer_.append_row(*datum_row, i))) {
//         if(ret == OB_ITER_END) {
//           ret = OB_SUCCESS;
//           pairs[macro_count].first = i;
//           pairs[macro_count].second = macro_count;
//           pair_p = &pairs[macro_count];
//           sstable_writer_.free_queue_.push(pair_p);
//           macro_count++;
//           if(OB_FAIL(sstable_writer_.full_queue_.pop(p, MaxTimeout))) {
//             LOG_WARN("TIMEOUT ERROR");
//           }
//           i = *(int*)p;
//           sstable_writer_.append_row(*datum_row, i);
//         } else {
//           LOG_WARN("fail to append row", KR(ret));
//         }
//       }
//     }
//   }
  for (int32_t i = 0; i < threads.size(); i++) {
    threads[i]->wait();
    threads[i]->destroy();
  }
  // sstable_writer_.open_ = false;
  // write_thread.wait();
  // write_thread.destroy();
  // for(int32_t i = 0; i < MaxMacroWriterNum; i++) {
  //   sstable_writer_.macro_block_writer_[i].close();
  // }
  // LOG_WARN("END_SSTABLE_WRITE");
  allocator.reset();
  // for(int i = 0; i < kConsumerNumSstable + AddAllocate; i++) {
  //   sstable_writer_.allocator_[i].reset();
  // }
  threads.clear();
  for(int i = 0;i < kConsumerNumSstable; i++) {
    mergers[i].reset();
  }
  if (OB_SUCC(ret)) {
    if (OB_FAIL(sstable_writer_.close())) {
      LOG_WARN("fail to close sstable writer", KR(ret));
    }
  }
 return OB_SUCCESS;
  
}

} // namespace sql
} // namespace oceanbase